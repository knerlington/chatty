var app = angular.module('modalModule', [])
.controller('modalCtrl', ["$scope", "$ionicModal", "$location", "authUser", "channelID", "$ionicPlatform", "firebaseRef", "$firebaseArray",
 function($scope, $ionicModal, $location, authUser, channelID,$ionicPlatform, firebaseRef, $firebaseArray) {
  console.log($scope);

  $ionicModal.fromTemplateUrl('partials/modal.html', {
    id: '1',
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
    $scope.toggle = true;
    $scope.active = 'tab1';
    $scope.firstTab = true;
    $scope.secondTab = false;
    $scope.thirdTab = false;
  });

// Firebase reference
  var ref = firebaseRef.getDB();

  var userId= authUser.userId;
  var userRef= ref.child("users/"+userId);

$scope.showChannelList = function() {
  $scope.showChannels = true;
  $scope.thirdTab = false; 
}


$scope.openModal = function() {
    if($scope.modal){
     $scope.modal.show(); 
    }
  };


$scope.closeModal = function() {
    $scope.modal.hide();
  };

$scope.showContacts = function(){
  $scope.closeModal();
  $location.path('/contacts');
  console.log("showContacts reached!");
}

$scope.showPrivateChannels = function() {
  console.log("showPrivateChannels reached!");
  $scope.closeModal();
  $location.path('/privateChannel');
}

$scope.goHome = function(){
  userRef.update({currentChannel:""});
  $scope.updateCounter();
  $scope.closeModal();
  $location.path('/channels');
}

$scope.hideChannels = function() {
  $scope.showChannels = false;
  $scope.showModal("tab3");
}

$scope.updateCounter = function(){
    // channel Ref
    var channelRef = ref.child("channels/"+channelID.id);
    channelRef.child("userCounter").transaction(function(Counter){
      return Counter-1;
    });
  }


$scope.showModal = function(modalName) {

  if (modalName === 'toolBox') {
      $scope.showThird = true;
      $scope.firstTab = true;
      $scope.showChannels = false;
      $scope.openModal();
  }


  if (modalName === 'tab1') {
      $scope.showThird = true; 

      $scope.firstTab = true;
      $scope.secondTab = false;
      $scope.thirdTab = false;
       $scope.showChannels = false;
  }

  if (modalName === 'tab2') {
      $scope.showThird = true; 

      $scope.firstTab = false;
      $scope.secondTab = true;
      $scope.thirdTab = false;
       $scope.showChannels = false;
  }


  if (modalName === 'tab3') {
      $scope.showThird = true; 

      $scope.firstTab = false;
      $scope.secondTab = false;
      $scope.thirdTab = true;
      $scope.showChannels = false;
  }


      $scope.openModal();

};




/*
$scope.active = function(num) {

    if(num == 0) {
      console.log(num + "hellothere number");
       return false;
    } else if (num == 0) {
       console.log(num + "hellothere number");
      return true;
    }
      

};

*/




// Added method to be called on logout. Uses method logout() in factory 'authUser' declared in loginModule.js
$scope.logOut = function() {
  console.log("Logout pressed!!!");
  $scope.closeModal();
  authUser.logout(channelID.id);
  $location.path('/login');
};

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });



  $scope.$on('modal.hidden', function() {

  });


  $scope.$on('modal.removed', function() {

  });
}]);


