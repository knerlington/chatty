//userModule
//used for handling the contacts
var app = angular.module("userModule", []);
var ref = null;
app.run(function($ionicPlatform){
	$ionicPlatform.on("deviceready", function(){
		
		console.log("FB in ionic ready:"+ref);
	});

});

app.controller("userController",["$scope", "$window","firebaseRef","$firebaseArray","authUser", "$firebaseObject","$location","infoUser",
	function($scope, $window,firebaseRef,$firebaseArray,authUser, $firebaseObject,$location,infoUser){

	$scope.ref = new Firebase("https://chat-chatty.firebaseIO.com");
	$scope.specificUser = $firebaseArray($scope.ref.child("users/"+authUser.userId));
	console.log("user:"+$scope.specificUser);
	$scope.contacts = $firebaseArray($scope.ref.child("users/"+authUser.userId+"/contacts"));

	//Contacts retrieval 
	//Iterates through the users contact list -
	//then through the user list on FB using contact.id.
	//The users found in the contact list are saved in local array finalContactList.
	//ngRepeat used on finalContactList.
	$scope.finalContactList = [];
	$scope.contacts.$loaded()
    .then(function(){
        angular.forEach($scope.contacts, function(contact) {
        	
            console.log(contact.id);
            $scope.contactFromUsers = $firebaseObject($scope.ref.child("users/"+contact.id));
            

            	$scope.finalContactList.push($scope.contactFromUsers);
            	console.log("contactFromUsers: "+$scope.contactFromUsers);	
            	
            
            
            
        })
    });
	
	$scope.showUserInfo = function(id){
		console.log(id);
		infoUser.update(id);
		$location.path("/userInfo");
	};


	$scope.getBack = function(){
		//$location.path('channels');
		$window.history.back();
	}

	
	$scope.userStatus = function(status){
		if(status){
			return "online";
		} else {
			return "offline";
		}
	}


}]);





