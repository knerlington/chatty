var app = angular.module("channelModule",[]);

app.factory("channelID", function(){
	'use strict';
	var currentChannel = {};
	currentChannel.update = function(id){
		this.id = id;
	};
	return currentChannel;
});

app.controller("channelCtrl",["$scope", "$location", "firebaseRef", "$firebaseArray", "channelID", "authUser","infoUser", "$ionicPlatform",
 function($scope,$location, firebaseRef,
 $firebaseArray, channelID, authUser, infoUser, $ionicPlatform){

	
	
	$scope.inputChannelName="";
	$scope.inputObj = {input: "",
		showChannelSearchList: false,
		showUserSearchList: false,
		userInput:""
	};
	
	$scope.inputUserName="";

	$scope.showChannelList= false;
	$scope.showUserList=false;
	$scope.searchChannel="";
	//Firebase reference 
	var ref = firebaseRef.getDB();
	var channelRef = ref.child("channels");
	$scope.channelList = $firebaseArray(channelRef);
	// user Id reference
	var userId= authUser.userId;
	var userRef= ref.child("users/"+userId);

	var allUserRef= ref.child("users");
	$scope.allUsers = $firebaseArray(allUserRef);

	$scope.nrOfMembers = "";					

	// Needed to create an empty object with property 'name' to sync with ng-model in the html file.
	// This was required as the channel name entered by the user could not be read in the code because it was null.
	// This is due to the addition of the ionic classes in the html file.
	$scope.channel = {};
	$scope.channel.name = "";
	$scope.createChannel= function (){
		console.log("Entered channel name: " + $scope.channel.name);
		$scope.channelList.$loaded().then(function() { // Added $loaded() to ensure that data is loaded before accessing values in the code.
			var found = false;
			for (var i=0; i<$scope.channelList.length; i++){
				console.log("chan name: "+$scope.channelList[i].channelName);
				if($scope.channelList[i].channelName === $scope.channel.name){
					found = true;
					$scope.channel.name = "";
					var message= "Kanalen finns redan, skapa en ny eller besök den existerande";
					navigator.notification.alert(
	              		message,   
	              		null,       
	              		"Vänligen notera!", 
	              		'OK' );					      
					return;
				}
			}

			if ($scope.channel.name.length > 0 && !found) // check to ensure that the channel name has a value and does not exist
			{
				$scope.channelList.$add({channelName:$scope.channel.name, userCounter:0}).then(function(channelRef){
					$scope.newChanId = channelRef.key();
					console.log("nya kanal id : "+ $scope.newChanId);
					$scope.joinChannel($scope.newChanId);
				});
				$scope.channel.name = "";
				console.log("createChannel() called!");
			}		
		});					
	}

	$scope.listChannels = function(){
		if($scope.showChannelList===false){
		$scope.showChannelList = true;
		} else {
		$scope.showChannelList = false;
		}
	}

	$scope.listUsers = function(){
		if($scope.showUserList===false){
			$scope.showUserList= true;
		} else {
			$scope.showUserList= false;
		}
	}

	$scope.joinChannel = function(id,add){
		if(!add){
			console.log("if !add****");
			var channelRef = ref.child("channels/"+channelID.id);
		    channelRef.child("userCounter").transaction(function(Counter){
		      return Counter-1;
		    });
		}
		$location.path('/chat');
		channelID.update(id);
		userRef.update({currentChannel:channelID.id});
		$scope.updateCounter(add);
	}


	$scope.goToUser = function(id){
		infoUser.update(id);
		$location.path('/userInfo');
	}

	$scope.userStatus = function(status){
		if(status){
			return "online";
		} else {
			return "offline";
		}
	}


//  döljer kanal-sök-listan om inget är skrivet i inputen
	$scope.channelSearchActive = function(){
		console.log("channelSearchActive() reached!");
		
		if($scope.inputObj.input === ""){
			$scope.inputObj.showChannelSearchList = false;
			console.log("showChannelSearchList: "+$scope.inputObj.showChannelSearchList);		
		} else{
			$scope.inputObj.showChannelSearchList = true;	
			console.log("showChannelSearchList: "+$scope.inputObj.showChannelSearchList);
		}

		
	}

// döljer användar--sök-listan om inget är skrivet i inputen
	$scope.userSearchActive = function(){
		console.log("userSearchActive reached!");
		

		if($scope.inputObj.userInput === ""){
			$scope.inputObj.showUserSearchList = false;
		
		} else{
			$scope.inputObj.showUserSearchList = true;	
		}
	}

	$scope.showChannelInputBool = false;
	$scope.showChannelInput = function() {
		if($scope.showChannelInputBool===false){
		$scope.showChannelInputBool = true;
		} else {
		$scope.showChannelInputBool = false;
		userRef.update({currentChannel:""});
		}
	}




	$scope.updateCounter = function(add){
		// channel Ref
		console.log("updateCounter");
		if(add){
			console.log("updateCounter if add");
			var channelRef = ref.child("channels/"+channelID.id);
			channelRef.child("userCounter").transaction(function(Counter){
			return Counter+1;
			});
		} else {
			console.log("updateCounter else");
		/*	var channelRef = ref.child("channels/"+channelID.id);
		    channelRef.child("userCounter").transaction(function(Counter){
		      return Counter-1;
		    }); */
		}
		
		
	}


	// get username
	var userName="";
	userRef.child("displayName").on("value", function(snapshot){
		userName = snapshot.val();
	});



}]);

