var app = angular.module("userInfoModule",[]);
var fb = null;

app.run(function($ionicPlatform){
	$ionicPlatform.on("deviceready", function(){
		

	});
});

app.factory("privateChannelID", function() {
	'use strict';
	var privateChannel = {};
	privateChannel.id = "";

	privateChannel.update = function(channelId) {
    	this.id = channelId;
  	};

  	return privateChannel;
});

app.controller("userInfoCtrl", ["$scope", "$location","$window", "infoUser", "authUser","firebaseRef","$firebaseArray", "$firebaseObject", "privateChannelID", 
	function($scope, $location,$window, infoUser, authUser, firebaseRef,$firebaseArray, $firebaseObject, privateChannelID) {
	
	$scope.userAlias="";
	$scope.userEmail="";

	$scope.userId = infoUser.userId;
	$scope.fb = new Firebase('https://chat-chatty.firebaseIO.com/users');
	$scope.contacts = $firebaseArray($scope.fb.child(authUser.userId+"/contacts"));

	$scope.ref = firebaseRef.getDB();


	// användar-info
	var userRef = $scope.ref.child("users/"+$scope.userId);
	$scope.userInfo = $firebaseObject(userRef);
	$scope.userInfo.$loaded().then(function(){
		$scope.userAlias= $scope.userInfo.displayName;
		$scope.userEmail = $scope.userInfo.email;
	});


	$scope.privateChat = function() {
		console.log("chatta med !");
		if (authUser.userId === infoUser.userId) {
			alert("Du försöker att prata med dig själv. Går inte!");
			//navigator.notfication.alert("Du försöker att prata med dig själv. Går inte!", null, "Prata privat", "OK");
		} else {
			var privateChannels = $firebaseArray($scope.ref.child("privateChannels/"));
			privateChannels.$loaded().then(function() {
				var channelFound = false;
				for (var i=0; i<privateChannels.length && !channelFound; i++) {
					if((privateChannels[i].members.creator == authUser.userId && privateChannels[i].members.recipient === infoUser.userId)||
						(privateChannels[i].members.creator == infoUser.userId && privateChannels[i].members.recipient === authUser.userId)) {
						privateChannelID.update(privateChannels[i].$id);
						console.log("Channel exists");
						$location.path('/privateChat');
						channelFound = true;
                    }
				}
				if(!channelFound) {
						console.log("Creating new channel");
						$scope.privateChannels = $firebaseArray($scope.ref.child("privateChannels"));
						$scope.privateChannels.$add({name:"privateChannel", members:{creator:authUser.userId, recipient:infoUser.userId}}).then(function(id){
						$scope.chanId = id.key();
						privateChannelID.update($scope.chanId);
						$scope.ref.child("users/"+ authUser.userId + "/myChannels").push({channelId:$scope.chanId});
						$scope.ref.child("users/"+ infoUser.userId + "/myChannels").push({channelId:$scope.chanId});
						$location.path('/privateChat');
					});					
				}
			});
		}
	}
	

	$scope.getBack = function(){
		$window.history.back();
	}


	$scope.addToContacts = function() {	
		var userFound = false;
		if (authUser.userId === infoUser.userId) { // Check to ensure that I do NOT add myself to my own contact list
			alert("Kan inte lägga till dig själv!");
		} else {
			$scope.contacts.$loaded().then(function(){
				$scope.contacts.forEach(function(item){
					if(item.id === infoUser.userId){
						userFound = true;
					}	
				});
				if (!userFound) {
					$scope.contacts.$add({id:infoUser.userId});
					alert($scope.userAlias + " har lagts till i din kontaktlista!");
				} else {
					alert($scope.userAlias + " finns redan i din kontaktlista!");
				}
			});
		}
		
	}

}]);