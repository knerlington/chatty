var app = angular.module('newAccountModule', ['firebase']);

app.controller('newAccountCtrl', ["$scope", "$firebase", "$location", "firebaseRef", "$firebaseObject", function($scope, $firebase, $location, firebaseRef, $firebaseObject) {

  $scope.user = {};
  $scope.user.displayName = "";
  $scope.user.email = "";
  $scope.user.password = "";
  $scope.user.passwordReEnter = "";
  $scope.user.currentChannel = "";

	var ref = firebaseRef.getDB();
	
  $scope.submitForm = function(isValid) {
    if (isValid) {
      // orderByChild() uses a .onIndex rule on displayName property in 'Security and Rules' in the firebase app.
      var userRef = ref.child("users").orderByChild("displayName").equalTo($scope.user.displayName);      
      // once() ensures that the callback method  is called just once and immediately removed.
      userRef.once("value", function(snapshot) {
        if (snapshot.val() === null) { // create user only if the sepcified display name is unique.      
          //----------------------------------------------------
          // TODO - need to set validation rule for this in firebase, but how????
          //----------------------------------------------------
          ref.createUser({
            email: $scope.user.email,
            password: $scope.user.password },
            function(error, userData) {
              $scope.$apply(function(){
                if (error) {
                  $scope.showError = true;
                  $scope.createUserError = error.message;
                } else {
                  console.log("userData: "+ userData);
                  console.log("Successfully created user account with uid:", userData.uid);
                  ref.child("users").child(userData.uid).set({
                    email: $scope.user.email,
                    displayName: $scope.user.displayName,
                    currentChannel: $scope.user.currentChannel
                  });
                  alert("Välkommen " + $scope.user.displayName + "! Nu kan du logga in.");
                  $location.path('/login');
                }
              });             
          });
        } else {
          $scope.$apply(function(){
            $scope.showDisplayNameError = true;
            $scope.sameDisplayNameError = "Användarnamnet är redan i användning. Var snäll och välj något annat.";
          });          
        }    
      });
    }
  };
	
  $scope.checkPasswords = function() {
		if (!($scope.user.password === $scope.user.passwordReEnter)) {
			$scope.passwordsNotEqual = true;
		} else {
			$scope.passwordsNotEqual = false;
		}
	};

}]);
