var app = angular.module('changePasswordModule', []);

app.controller('changePasswordCtrl', ["$scope", "$firebase", "$location", "firebaseRef", function ($scope, $firebase, $location, firebaseRef ) {
	
	var ref = firebaseRef.getDB();

	$scope.user = {};  
  	$scope.user.email = "";
  	$scope.user.tempPassword = "";
  	$scope.user.password = "";
  	$scope.user.passwordReEnter = "";
  
	$scope.submitForm = function(isValid) {
  		if (isValid) {
  			ref.changePassword({
				email       : $scope.user.email,
			  	oldPassword : $scope.user.tempPassword,
			  	newPassword : $scope.user.password
				}, 
				function(error) {
					if (error === null) {
	    				console.log("Password changed successfully");
	    				alert("Lösenordet är återställt!");
	    				$scope.$apply(function() {
	    					$location.path('/channels');
	    				});	    				
	  				} else {
	    				console.log("Kunde inte återställa lösenordet:", error);
	    				$scope.showError = true;
						$scope.resetPasswordError = "Kunde inte återställa lösenordet:" + error;
	  				}	  	
				}
			);
  		} 
  	};

  	function onChangePasswordError() {

  	}

  	$scope.checkPasswords = function() {
		if (!($scope.user.password === $scope.user.passwordReEnter)) {
			$scope.passwordsNotEqual = true;
		} else {
			$scope.passwordsNotEqual = false;
		}
	};

 }]);