var app = angular.module("privateChannelModule",[]);

//initialization after all the modules have been loaded
//calling on("deviceready") and performing work here guarantees that ionic and the device libraries have been loaded before
//we try to access any device APIs/plugins
//use $ionicPlatform.on("cordova event", callbackFn) to be sure that ionic doesn't mess anything up  
app.run(function($ionicPlatform, $window, $location){
	$ionicPlatform.on("deviceready", function(){
		
	});
});


app.controller("privateChannelCtrl",["$scope", "$location", "firebaseRef", "$firebaseArray", "privateChannelID", "authUser","infoUser","$firebaseObject",
 "privateChannelID",function($scope,$location, firebaseRef,
 $firebaseArray, privateChannelID, authUser, infoUser, $firebaseObject, privateChannelID){

 	$scope.ref = firebaseRef.getDB();
 	$scope.privateChannelsRef = $scope.ref.child("privateChannels");
 	
 	$scope.localChannelArray = [];
 	$scope.privateChannels = $firebaseArray($scope.privateChannelsRef);
 	$scope.privateChannels.$loaded().then(function() {
 		$scope.privateChannels.forEach(function(item){
 			if(item.members.creator === authUser.userId){
 				var userRef = $scope.ref.child("users/"+ item.members.recipient);
				var userObj = $firebaseObject(userRef);
				$scope.localChannelArray.push({info:userObj, channelId:item.$id});	
 			} else if (item.members.recipient === authUser.userId) {
 				var userRef = $scope.ref.child("users/"+ item.members.creator);
				var userObj = $firebaseObject(userRef);
				$scope.localChannelArray.push({info:userObj, channelId:item.$id});
 			}			
 		});
    });

    
    $scope.redirectToPrivateChannel = function(channelId){
    	privateChannelID.update(channelId);
    	$location.path("/privateChat");
    }


 	$scope.keyCodeCheck = function(event){
		if(event.keyCode === 13){
			console.log("keyCodeCheck reached!");
			$scope.sendButton();
		}
	}	

}]);

