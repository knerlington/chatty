
var app = angular.module("mainModule", ['firebase',"ngRoute", "loginModule", "newAccountModule", 
	"userModule","channelModule", "chatModule", "modalModule","userListModule", "userInfoModule", "changePasswordModule", "forgottenPasswordModule", "privateChatModule","privateChannelModule", "ionic"]);

//the app configuration
//injects the ngRouteProvider service available in the module ngRoute which always checks the browsers current URL
//the when() method takes two params. the first one is the path on which it should set the partial and the second
//is an object that can take several keys. templateURL is the partial to be used and the controller is the logic to use
//for example: if I'm at the root of the web page "/" I want to use the main site index.html. when index.html is active
// I want to use the mainController.
app.config(["$routeProvider", function($routeProvider){
	  // $ionicPlatform.on("deviceready", function(){
	  // 	$ionicPlatform.registerBackButtonAction(function(){
   //    console.log("$ionicPlatform reached!");
   //    if($location.path()!="/channels" && $location.path()!="/login"){
   //      $window.history.back(); 
   //    }
      
   //  }, 100);	
	  // });

	


	  
	$routeProvider.
	//when("/", {templateUrl: "partials/root.html", controller: "mainController"}).
	when("/login", {templateUrl: "partials/login_rc.html", controller: "loginCtrl"}).
	when("/newAccount", {templateUrl: "partials/newaccount_rc.html", controller: "newAccountCtrl"}).
	when("/changePassword", {templateUrl: "partials/changePassword.html", controller: "changePasswordCtrl"}).
	when("/forgottenPassword", {templateUrl: "partials/forgottenPassword.html", controller: "forgottenPasswordCtrl"}).
	when("/channels", {templateUrl: "partials/channels.html", controller: "channelCtrl"}).
	when("/chat", {templateUrl: "partials/chat.html", controller: "chatCtrl"}).
	when("/userInfo", {templateUrl: "partials/userInfo.html", controller: "userInfoCtrl"}).
	when("/userList", {templateUrl: "partials/userList.html", controller: "userListCtrl"}).
	when("/contacts", {templateUrl: "partials/contacts.html", controller: "userController"}).
	when("/privateChat", {templateUrl: "partials/privateChat.html", controller: "privateChatCtrl"}).
	when("/privateChannel", {templateUrl: "partials/privateChannels.html", controller: "privateChannelCtrl"}).
	otherwise({redirectTo: "/login"});
}]);

//initialization after all the modules have been loaded
//calling on("deviceready") and performing work here guarantees that ionic and the device libraries have been loaded before
//we try to access any device APIs/plugins
//use $ionicPlatform.on("cordova event", callbackFn) to be sure that ionic doesn't mess anything up  
app.run(function($ionicPlatform, $window, $location){
	$ionicPlatform.on("deviceready", function(){
		console.log("deviceready!");
		//overrides androids back button
		$ionicPlatform.registerBackButtonAction(function(){ 
			console.log("back button pressed!");
			if($location.path() != "/login" && $location.path() != "/channels"){
				$window.history.back();
			}
		}, 1000);

		
	});
});
//custom service 
//fetches the firebase ref only
app.factory("firebaseRef", [function(){

	return {
		getDB: function(){
			var ref = new Firebase("https://chat-chatty.firebaseIO.com");
			return ref;
		}
	};
}]);

app.controller("mainController", ["$scope", "$location", "$routeParams", function($scope, $location, $routeParams){
	
	
		var currentURL = $location.path();
		console.log(currentURL);
		console.log("mainController running!");
		$scope.greeting = "Greetings!";
		$scope.changeUrl = function(){
			$location.path("/login");
			
		};

}]);

app.controller("loginController", ["$scope","$routeParams", function($scope, $routeParams){
	

		$scope.greeting = "Greetings!";
	

}]);



