var app = angular.module("chatModule",[]);

//initialization after all the modules have been loaded
//calling on("deviceready") and performing work here guarantees that ionic and the device libraries have been loaded before
//we try to access any device APIs/plugins
//use $ionicPlatform.on("cordova event", callbackFn) to be sure that ionic doesn't mess anything up  
app.run(function($ionicPlatform, $window, $location){
	$ionicPlatform.on("deviceready", function(){
		
	});
});

app.controller("chatCtrl",["$scope", "$location", "firebaseRef", "$firebaseArray", "$firebaseObject" ,"channelID", "authUser",
"$window", "$ionicPlatform", "$ionicScrollDelegate", "$anchorScroll", "$timeout", "$document", "infoUser",
 function($scope, $location, firebaseRef, $firebaseArray, $firebaseObject, channelID, authUser, $window, $ionicPlatform, $ionicScrollDelegate, $anchorScroll, $timeout, $document, infoUser){

	$scope.scrollToBottom = function (){
		console.log("in scrollToBottom");
		console.log("scroll height: " + document.body.scrollHeight);

		//Scrolls content to bottom when messages are loaded. 
		$ionicScrollDelegate.scrollBottom();

		window.scrollTo(0, document.body.scrollHeight);
  	};

 	
	$scope.alias = "";
	$scope.message= "";

	// Firebase reference
	var ref = firebaseRef.getDB();
	var messagesInChannel = ref.child("messages/"+channelID.id);
	console.log("messinchan: "+messagesInChannel);
	$scope.messages= $firebaseArray(messagesInChannel);

	var thisChanRef= ref.child("channels/"+channelID.id);
	$scope.thisChannelName="";
	thisChanRef.child("name").on("value", function(snapshot){
		$scope.thisChannelName = snapshot.val();
	});
	

	// Get channel object - to display channel name in title
	$scope.channel = $firebaseObject(ref.child("channels/"+channelID.id));


	$scope.tempID = channelID.id;

	// user ID reference
	var userId= authUser.userId;
	var userRef= ref.child("users/"+userId);
	
	var userObject = $firebaseObject(userRef);
	// Firebase values are loaded asynchronously. Therefore if we need to use the values in our code we need to call 
	// $loaded to get a promise which ensures that our code is only executed once the data from firebase is actually loaded. 
	userObject.$loaded().then(function () {
		$scope.userName =  userObject.displayName;
	});
	
	$scope.inputMessage="";
	$scope.date="";

	$scope.keyCodeCheck = function(event){
		if(event.keyCode === 13){
			console.log("keyCodeCheck reached!");
			$scope.sendButton();
		}
	};
	
	
	$scope.sendButton = function() {	

		$scope.messageRef = ref.child("messages/"+channelID.id);
		// datum och tid
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var hour = today.getHours();
		var min = today.getMinutes();
		var sec = today.getSeconds();

		if(dd<10) {
		    dd='0'+dd
		} 
		if(mm<10) {
		    mm='0'+mm
		} 
		if(hour<10) {
		    hour='0'+hour
		} 
		if(min<10) {
		    min='0'+min
		} 
		if(sec<10) {
		    sec='0'+sec
		} 

		today = dd+'/'+mm+" Kl."+hour+":"+min;
		
		$scope.messageRef.push({alias:$scope.userName, message:$scope.inputMessage, date:today});
		// Scrolls to the last message when a message is added. 
		$ionicScrollDelegate.scrollBottom(true);
		// rensar inputfältet
		$scope.inputMessage="";
		
	}


	$scope.userList = function(){
		$location.path('/userList');
	}

	$scope.getBack = function(){
		userRef.update({currentChannel:""});
		$scope.updateCounter();
		$window.history.back();
	}

	$scope.updateCounter = function(){
		var channelRef = ref.child("channels/"+channelID.id);
		channelRef.child("userCounter").transaction(function(Counter){
			return Counter-1;
		});
	}					

}]);


