var app = angular.module("userListModule", []);

app.factory("infoUser", function() {
	'use strict';
	var selectedUser = {};
	selectedUser.userId = "";

	selectedUser.update = function(userId) {
    	this.userId = userId;
  	};

  	return selectedUser;
});


app.controller("userListCtrl", ["$scope","$location","$window", "firebaseRef", "$firebaseArray", "channelID", "infoUser",
	function($scope,$location, $window, firebaseRef, $firebaseArray, channelID, infoUser){

$scope.channelID = channelID.id;



//Firebase reference
var ref = firebaseRef.getDB();
var usersInChannel = ref.child("users").orderByChild("currentChannel").equalTo(channelID.id);
$scope.users= $firebaseArray(usersInChannel);

console.log("strl: "+$scope.users.length);


$scope.userInfo = function(index){
	//console.log("user id: "+ $scope.users[index].$id);
	console.log("logga info om : "+ $scope.users[index].displayName);
	infoUser.update($scope.users[index].$id);
	$location.path('userInfo');
}

$scope.getBack = function(){
		//$location.path('/chat');
		$window.history.back();
	}

}]);