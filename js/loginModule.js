var app = angular.module('loginModule', []);

app.factory('authUser', function() {
  'use strict';
  var authInfo = {};
  authInfo.userId = "";
  authInfo.mainRef = "";
  authInfo.userRef = "";

  authInfo.update = function(userId, mainRef, userRef) {
    this.userId = userId;
    this.mainRef = mainRef;
    this.userRef = userRef;
  };

  authInfo.logout = function(channelId) {
    var data;
    this.mainRef.unauth();
    console.log("Client unauthenticated.");
    this.mainRef.onAuth(function(authData) {
      data = authData;
    });
    if (!data) {    
        this.userRef.set(false);
    }
    if (channelId) {
        var channelRef = new Firebase('https://chat-chatty.firebaseIO.com/channels/' + channelId);
        var userCurrentChannelRef = new Firebase('https://chat-chatty.firebaseIO.com/users/' + this.userId);
        channelRef.child("userCounter").transaction(function(Counter){
          return Counter-1;
        });
        userCurrentChannelRef.update({"currentChannel":""});
    } 
  };

  return authInfo;     
});

app.controller('loginCtrl', ["$scope", "$firebase", "$location", "authUser", "firebaseRef",
  function ($scope, $firebase, $location, authUser, firebaseRef) {  
  
  $scope.ref = firebaseRef.getDB();
  $scope.connectedUsers = new Firebase('https://chat-chatty.firebaseIO.com/.info/connected');

  $scope.user = {};
  $scope.user.email = "";
  $scope.user.password = "";
      
    // ------------------- Facebook login --------------------
    /*$scope.login = function(){
        console.log("login function");
        ref.authWithOAuthPopup("facebook", function(error, authData) {
        if (error) {
          console.log("Login Failed!", error);
        } else {
          // the access token will allow us to make Open Graph API calls
          console.log(authData.facebook.accessToken);
        }
      });
    };*/

  $scope.submitForm = function(isValid) {
    if(isValid) {
      $scope.ref.authWithPassword({
        email    : $scope.user.email,
        password : $scope.user.password          
      }, function(error, authData) {
        $scope.$apply(function() {
          if (error) {
            console.log("Login Failed!", error);
            $scope.showError = true;
            $scope.loginError = error.message;
          } else {
            console.log("Authenticated successfully with payload:", authData);
            $scope.userRef = new Firebase('https://chat-chatty.firebaseIO.com/users/' + authData.uid + '/online');
            authUser.update(authData.uid, $scope.ref, $scope.userRef);
            if (authData.password.isTemporaryPassword) {
              console.log("temporary token");
              $location.path('/changePassword');
            } else {
              $scope.setOnlineStatus();
              console.log("normal password token");
              $location.path('/channels');
            }              
          }
        });        
      });       
    } else {
      console.log("form is inalid");
      event.preventDefault();
    }  
  };

  $scope.setOnlineStatus = function() {
    $scope.connectedUsers.on('value', function(snapshot) {
      if (snapshot.val()) {
        $scope.userRef.onDisconnect().set(false);
        $scope.userRef.set(true);
      }
    });
  };

}]);